"""Command line interface for Fantasy Name Generator.

MIT License

Copyright (c) 2019 Federico Salerno <itashadd+fnamegen[at]gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from typing import List
from fng import REGIONS


def prompt_mode() -> str:
    """Prompt for a mode and return it or `exit`."""
    regions = "\n".join(region.capitalize() for region in REGIONS.keys())

    return input(f"Choose a region from...\n"
                 f"{regions}\n"
                 f"...or `exit`: ")


def pretty_print(names_list: List[str], per_line: int = 10) -> str:
    """Return a pretty-printed string from a string of names.

    :param names_list: list of names to prepare.
    :param per_line: number of names per line. Default = 10.
    """

    padding = max(map(len, names_list))

    # Line break at every interval.
    output = "".join([n if i == len(names_list)
                      else f"{n}\n" if i != 0 and i % per_line == 0
                      else f"{n}{' ' * (padding - len(n))} | "
                      for i, n in enumerate(names_list, start=1)])

    # If there's more than one line, start with a new line.
    return f"\n{output}" if "\n" in output else output


if __name__ == '__main__':
    last_input = ""
    last_mode = ""
    last_gender = ""
    names_amount = 1

    while last_input.lower() != "exit":
        names = []
        if last_mode and not last_input:
            names = [name for name in REGIONS[last_mode].get_names(amount=names_amount, gender=last_gender)]
        else:
            last_mode = ""

            if not last_input:
                last_input = prompt_mode()

            if last_input.lower() in REGIONS.keys():
                try:
                    names_amount = int(input("How many? "))
                except ValueError:
                    names_amount = 1

                last_gender = input("What gender? (`m`, `f` or blank for any) ")

                names = [name for name
                         in REGIONS[last_input.lower()].get_names(amount=names_amount, gender=last_gender)]
                last_mode = last_input.lower()
            else:
                last_input = ""
                last_mode = ""
                last_gender = ""
                names_amount = 1
                continue

        if names:
            last_input = input(f"Names generated: {pretty_print(names)}\n"
                               f"Enter blank for more, a region name to change settings, `exit` to exit: ")
