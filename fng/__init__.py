from fng.constants import LETTERS, SYMBOLS, VOWELS, CONSONANTS
from fng.regions import REGIONS

__all__ = ["LETTERS", "SYMBOLS", "VOWELS", "CONSONANTS", "REGIONS"]
