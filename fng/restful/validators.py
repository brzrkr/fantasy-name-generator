"""Validators for user input in API parameters."""

from typing import Optional
from fng.regions import REGIONS

MINIMUM_AMOUNT: int = 10
"""Minimum amount of names to accept for each request.

A larger minimum might be more than the user needs but
reduces server load and, possibly, abuse.
"""

MAXIMUM_AMOUNT: Optional[int] = 1000
"""Maximum amount of names to accept for each request.

None = no limit. A large maximum might cause heavy load
on the server.
"""


def allowed_amount(u_input: int) -> bool:
    """Check whether input fits minimum and maximum limits."""
    return u_input >= MINIMUM_AMOUNT and (MAXIMUM_AMOUNT is None or u_input <= MAXIMUM_AMOUNT)


def allowed_gender(u_input: str, region: str) -> bool:
    """Check whether the inputted gender is allowed by the selected region."""
    return u_input.lower() in getattr(REGIONS[region.lower()], "supported_genders", []) or u_input == ""
