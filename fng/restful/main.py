"""REST-ful API for Fantasy Name Generator

MIT License

Copyright (c) 2019 Federico Salerno <itashadd+fnamegen[at]gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from flask import Flask, make_response
from flask_restful import Api, Resource
from webargs import fields
from webargs.flaskparser import use_kwargs
from fng.restful import validators as validate
from fng.regions import REGIONS

app = Flask(__name__)
api = Api(app)

# Collect fields in distinct dicts so they can be mixed-in.
# Except for the `amount` field, all options should have the
# same key name as the region classes expect.

# noinspection PyUnresolvedReferences
common_options = {
    "n": fields.Int(required=False, attribute="amount", allow_none=True, default=None, missing=None),
}
# noinspection PyUnresolvedReferences
gendered = {
    # An empty gender option results in a randomly picked one.
    "gender": fields.String(required=False, missing=""),
}


# noinspection PyMethodMayBeStatic
class Index(Resource):
    def get(self):
        # TODO: Make explanation page, read from it.
        # TODO: Include supported options for each region.
        regions_list = "\n".join(region for region in REGIONS.keys())
        body = f"Available regions:\n{regions_list}"
        output = make_response(body, 200)
        output.headers["Content-Type"] = "application/json"
        return output


# noinspection PyMethodMayBeStatic
class Anthura(Resource):
    @use_kwargs({
        **common_options,
        **gendered,
    })
    def get(self, amount: int = None, **kwargs):
        if amount is None:
            output = make_response("Specify an amount with /anthura?n={amount}\n"
                                   "You may also specify a gender with &gender={m|f}", 200)
            output.headers["Content-Type"] = "application/json"
            return output  # TODO: Explain usage/options properly.

        # Validate input.
        if not validate.allowed_amount(amount):
            output = make_response("The amount is too big or too small.", 400)
            output.headers["Content-Type"] = "application/json"
            return output  # TODO: handle validation error properly.

        if not validate.allowed_gender(kwargs["gender"], "anthura"):
            output = make_response(f"Gender {kwargs['gender']} is not supported.", 400)
            output.headers["Content-Type"] = "application/json"
            return output  # TODO: handle validation error properly.

        names = [name for name in REGIONS["anthura"].get_names(amount, **kwargs)]

        output = make_response(str(names), 200)
        output.headers["Content-Type"] = "application/json"

        return output


api.add_resource(Index, "/")
api.add_resource(Anthura, "/anthura")

if __name__ == "__main__":
    app.run(debug=True)
