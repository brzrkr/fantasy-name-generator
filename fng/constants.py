"""Constants for fantasy-name-generator."""

from typing import Sequence
import numpy as np
from numpy import array

# Forcing dtype="object" avoids numpy.str_ for the characters,
# which would be problematic when turning arrays back to Python containers.
VOWELS: Sequence[str] = array(tuple("euioaöü"), dtype="object")
"""Characters to be considered vowels."""

CONSONANTS: Sequence[str] = array(tuple("qwrtypsdfghjklzxcvbnm"), dtype="object")
"""Characters to be considered consonants."""

LETTERS: Sequence[str] = np.union1d(VOWELS, CONSONANTS)
"""Characters to be considered letters."""

SYMBOLS: Sequence[str] = array(tuple("-'"), dtype="object")
"""Symbols to be included in generation algorithms."""
