"""Rulesets (regions) for name generation.

MIT License

Copyright (c) 2019 Federico Salerno <itashadd+fnamegen[at]gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from collections import defaultdict
from fng.constants import LETTERS, VOWELS, CONSONANTS
import numpy as np
from numpy import array
from numpy.random import choice
from typing import Optional, Dict, Iterator
from abc import ABC, abstractmethod


class Region(ABC):
    """Parent class for name generation rulesets.

    Must be inherited, not instantiated directly.
    All Regions expose the get_names() method to generate names
    and have numpy arrays representing sets of allowed characters.
    """

    letters_per_word: array
    """Allowed amounts of letters per name (not strict)."""

    letters_per_word_p: array
    """Probabilities of each amount of letters per name to be chosen.
    
    Must have the same length of letters_per_word.
    """

    allowed_letters: array
    """Letters allowed by the ruleset. Must be a subset of constant LETTERS."""

    allowed_vowels: array
    """Vowels allowed by the ruleset. Must be a subset of allowed_letters."""

    allowed_consonants: array
    """Consonants allowed by the ruleset. Must be a subset of allowed_letters."""

    allowed_symbols: array
    """Symbols allowed by the ruleset. Must be a subset of constant SYMBOLS."""

    # noinspection PyUnusedLocal
    @classmethod
    @abstractmethod
    def _make_name(cls, **kwargs) -> str:
        """Generate one name. Not meant to be called from outside the class."""
        return NotImplemented

    @classmethod
    def get_names(cls, amount: int = 1, **kwargs) -> Iterator[str]:
        """Generate one or more names from this region.

        :param amount: number of names to generate. Default = 1.
        :param kwargs: any additional option for the name.
        :return: names as a generator.
        """

        for _ in range(amount):
            yield cls._make_name(**kwargs)


class Anthura(Region):
    """The rules for this region are based on the homonymous fictional
    region created by writer Giovanni Attanasio and are developed with
    his permission. You are not allowed to modify this without permission.
    """

    letters_per_word = array((4, 6))
    letters_per_word_p = array((0.45, 0.55))

    allowed_letters = np.setdiff1d(LETTERS, tuple("qwyjxpbzk"), assume_unique=True)
    allowed_vowels = np.intersect1d(VOWELS, allowed_letters, assume_unique=True)
    allowed_consonants = np.intersect1d(CONSONANTS, allowed_letters, assume_unique=True)

    final_vowels = np.intersect1d(allowed_vowels, tuple("aeou"), assume_unique=True)

    gendered_vowels = defaultdict(lambda: Anthura.final_vowels)
    gendered_vowels.update({
        "m": tuple("ou"),
        "f": tuple("ae"),
    })
    supported_genders = array([gl for gl in gendered_vowels.keys()], dtype="object")

    can_be_double = np.array(tuple("l"))
    can_precede_consonant = np.array(tuple("rnmls"))
    can_pre_cons_start = np.intersect1d(can_precede_consonant, tuple("s"))

    @classmethod
    def _make_name(cls, gender: Optional[str] = None) -> Iterator[str]:
        """Generate one name.

        :param gender: m for male names, f for female names, None for any.
        """

        # If no gender was chosen, pick one at random.
        gender = gender \
            if gender in cls.supported_genders \
            else choice(cls.supported_genders)

        # Pick number of letters.
        n_letters = choice(cls.letters_per_word, p=cls.letters_per_word_p)

        # Build the name one letter at a time
        name = ""
        while len(name) < n_letters or name.endswith(tuple(CONSONANTS)):
            last_letter = name[-1] if len(name) >= 1 else ""
            s_last_letter = name[-2] if len(name) >= 2 else ""

            # By default, all allowed letters can be picked.
            letter_pool = cls.allowed_letters.copy()

            # First letter is always a consonant.
            if len(name) == 0:
                name += choice(cls.allowed_consonants)
                continue

            # Ensure that the name ends with a final vowel depending on gender.
            # If it doesn't end in a consonant, wait the next cycle.
            if len(name) >= n_letters and last_letter in CONSONANTS:
                name += choice(np.intersect1d(cls.final_vowels, cls.gendered_vowels[gender], assume_unique=True))
                break

            # Generally, consonants are followed by vowels.
            if last_letter in CONSONANTS:
                # The last vowel has to fit a specific subset.
                if len(name) == n_letters - 1:
                    letter_pool = np.intersect1d(cls.final_vowels, cls.gendered_vowels[gender], assume_unique=True)
                else:
                    letter_pool = np.intersect1d(letter_pool, VOWELS)
                    # At the beginning of a name, only masculine names can have two consonants.
                    if len(name) == 1:
                        # And the first consonant must fit a specific subset.
                        if last_letter in cls.can_pre_cons_start:
                            letter_pool = np.union1d(letter_pool, cls.allowed_consonants)
                    else:
                        # In the middle of a word, some consonants can precede other consonants.
                        if s_last_letter not in CONSONANTS \
                                and last_letter in cls.can_precede_consonant:
                            letter_pool = np.union1d(letter_pool, cls.allowed_consonants)

                    # But only some consecutive consonants can be double.
                    if last_letter not in cls.can_be_double:
                        letter_pool = np.setdiff1d(letter_pool, np.array(last_letter))
            else:
                # Vowels cannot be followed by other vowels.
                letter_pool = np.intersect1d(letter_pool, CONSONANTS)

            name += choice(letter_pool)

        return name.capitalize()


# Register all valid regions and corresponding rulesets.
REGIONS: Dict[str, Region] = {name.lower(): cls for name, cls in
                              zip(locals().keys(), locals().values())
                              if isinstance(cls, type)
                              and issubclass(cls, Region)
                              and cls is not Region}
"""Dictionary of region names and corresponding ruleset classes for supported regions."""
