# Fantasy Name Generator
This fantasy name generator creates person names from a set of 
rules (they don't really have to be fantasy names!),
instead of picking from a list of predefined names as most
(lazy) competitors do. See below (Regions) for info on how the
names are generated. 

**To use Fantasy Name Generator**, a command line interface or a
REST-ful API are provided. Or you can just `import fng.regions` and
implement your own interface through Python.

See below for usage notes of predefined interfaces.

## Usage
### Command Line Interface
The CLI is contained in the `fng/cli` directory. Just run the
`main.py` script and follow the instructions to generate as many names
as you want, using a predefined "region" (see below) and any options
it implements.

### REST-ful API
The REST-ful API lies in the `fng/restful` directory and is based on
Flask/Flask-restful; the server side is run with `main.py`.

- To check available rulesets and endpoints, make a GET request to the
index endpoint `https://<host>/`.
- To check available options for each ruleset, make a GET request
to the desired ruleset's endpoint `https://<host>/<region>`.
- To generate names, make a GET request to the desired ruleset's
endpoint with the desired options, specifying at least the amount of
names to generate `https://<host>/<region>?n=10`.

## Common options
The following options are common to all rulesets. Any option that is
not recognised by the ruleset will be ignored.

- `n`: number of names to generate. A minimum amount may be enforced by
the host to reduce the number of requests made. By default, a minimum of
10 is set. Edit this rule in the `restful/validators.py` module.

## Regions (rulesets)
Fantasy Name Generator uses self-contained rulesets – called Regions,
to simulate different shared origins for the names and determine the
shape and feel of the names themselves.

Names are generated procedurally following rules such as "which letters
can appear first", "which consonants cannot appear in this region",
"how long is the name" and so on.

The words generated could, while still obeying the rules of each region,
not correspond to human "aesthetic requirements". In other words, they
may suck. Pick the ones you like and ignore the others.

The default rulesets below are derived from the fictional work of Italian
author Giovanni Attanasio. They are free to be used directly from this
program but modification is disallowed without explicit permission.

### Anthura
*Example names:* Nana, Nümame, Lucena, Mifolu, Tusohu, Döru.

**Additional options:**
- `gender`: one of `m`, `f`. Gender of the names to generate. If not
specified, a gender will be picked at random for each name, resulting in a
mixed genders list.

Names from Anthura are gendered, can be between 4 and 7 letters, always
begin with consonants and always end with vowels. The stress is uncertain.

Vowels cannot be adjacent to each other, only some combinations of
consonants can form clusters of maximum two, and only `l` can be double.

Male names end in either `u` or `o`, female names end in either `a` or `e`.
